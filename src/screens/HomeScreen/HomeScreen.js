import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, Text, View, ScrollView, ImageBackground } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { Dimensions } from 'react-native';
import { connect } from 'react-redux';
import StoreList from './StoreList.js'

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH);

const DATA = [
  "https://atzone.in/wp-content/uploads/2019/10/Amazon-deals-banner.jpg",
  "https://atzone.in/wp-content/uploads/2019/10/Amazon-deals-banner.jpg",
  "https://atzone.in/wp-content/uploads/2019/10/Amazon-deals-banner.jpg",
  "https://atzone.in/wp-content/uploads/2019/10/Amazon-deals-banner.jpg",
  "https://atzone.in/wp-content/uploads/2019/10/Amazon-deals-banner.jpg",
];

function _renderItem({ item }) {
  return (
      <ImageBackground
        source={item}
        style={styles.image}
        imageStyle={styles.imageStyle}
      >
        <View></View>
      </ImageBackground>
  );
}

const categories = [
  {
    category: 'MyStores',
    categoryName: 'My Stores',
  },
  {
    category: 'NearGnenerals',
    categoryName: 'General Stores Near You',
  },
  {
    category: 'NearFarmacy',
    categoryName: 'Farmacies Near You',
  },
  {
    category: 'MyStores1',
    categoryName: 'My Stores',
  },
  {
    category: 'NearGnenerals1',
    categoryName: 'General Stores Near You',
  },
  {
    category: 'NearFarmacy1',
    categoryName: 'Farmacies Near You',
  }
];

function HomeScreen({navigation}) {
  const [index, setIndex] = useState(0);
  let myCorousel = useRef();
  myCorousel.current && myCorousel.current.focus()
  useEffect(() => {
  
  });

  return (
    <View style={styles.corouselContainer}>
      <View style={styles.offerCorousel}>
        <Carousel
          ref={(c) => myCorousel = c}
          data={DATA}
          renderItem={_renderItem}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          containerCustomStyle={styles.corousel}
          inactiveSlideShift={0}
          onSnapToItem={(index) => setIndex(index)}
          autoplay={true}
          loop={true}
          useScrollView={true}
        />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        {
          categories.map((category, index) =>
            <StoreList navigation={navigation} category={category} key={index}></StoreList>
          )
        }
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  corouselContainer: {
    flex: 1,
    alignSelf: 'stretch',
  },
  offerCorousel: {
    height: 150,
    alignSelf: 'stretch',
    backgroundColor: 'white',
  },
  corousel: {

  },
  image: {
    flex: 1,
    justifyContent: "center",
    alignSelf: 'stretch',
  },
  imageStyle: {
    resizeMode: "cover",
    alignSelf: 'stretch',
  },
});

const mapStateToProps = (state, props) => {
  const { activeProduct } = state;
  return {
    activeProduct,
  };
}

const mapDispatchToProps = (dispatch) => ({
  getStoresByPincode: (queryDetails, callback) => dispatch(storeActions.getStoresByPincode(queryDetails, callback)),
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);