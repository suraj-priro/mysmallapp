import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native';

export default function StoreImage(props) {
    useEffect(() => {
    });

    return (
        <View style={styles.ImageContainer} onClick={()=>props.navigation.navigate('MyStores')}>
            {
                props.store ?
                    <ImageBackground
                        source={props.store.imageUri}
                        style={styles.image}
                        imageStyle={styles.imageStyle}
                    >
                        <View style={styles.overlay}>
                            <Text style={styles.storeName}>{props.store.storeName}</Text>
                        </View>
                    </ImageBackground>
                    : null
            }
        </View>
    )
}

const styles = StyleSheet.create({
    ImageContainer: {
        marginLeft: 3,
        marginRight: 3,
        width: 70,
    },
    image: {
        flex:1,
        alignSelf:'stretch',
        
    },
    imageStyle: {
        borderRadius: 5,
        resizeMode: 'cover',
        alignSelf: 'stretch',
    },
    overlay: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'stretch',
    },
    storeName: {
        backgroundColor: 'orange',
        padding: 2,
        color: 'white',
        fontSize: 8,
        textAlign: 'center',         
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
    },
});
