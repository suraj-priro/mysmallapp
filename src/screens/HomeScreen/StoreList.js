import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import StoreImage from './StoreImage'


const storeList = [
    {
        imageUri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSC1vGK9o02QL8RT9TOeWbRJeExswc2NAbTdMWG47brlibVlPBM&usqp=CAU',
        storeName: 'Pawan Store',
    },
    {
        imageUri: 'https://images.jdmagicbox.com/comp/delhi/n1/011pxx11.xx11.121217160731.t7n1/catalogue/vijay-store-kamla-nagar-delhi-general-stores-3ko4xcj.jpg?clr=#808080',
        storeName: 'Kishore Store',
    },
    {
        imageUri: 'https://www.kantarretailiq.com/Content/Render/1489183.jpeg',
        storeName: 'Lucky Store',
    },
    {
        imageUri: 'https://www.kantarretailiq.com/Content/Render/1489183.jpeg',
        storeName: 'Balaji Store',
    },
    {
        imageUri: 'https://www.kantarretailiq.com/Content/Render/1489183.jpeg',
        storeName: 'Medic Store',
    },
    {
        imageUri: 'https://www.kantarretailiq.com/Content/Render/1489183.jpeg',
        storeName: 'Karan Store',
    },
];


export default function StoreList(props) {
    useEffect(() => {

    } );

    return (
        <View style={styles.StoreCategory}>
        <Text style={styles.categoryName}>{props.category && props.category.categoryName}</Text>
        <ScrollView style={styles.categoryContainer} horizontal showsHorizontalScrollIndicator={false}>
            {
                storeList.map((store, index) =>
                <StoreImage navigation={props.navigation} key={index} store={store}></StoreImage>
                )
            }
        </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    StoreCategory: {
        marginBottom: 4,
    },
    categoryName: {
        marginLeft: 10,
        marginTop: 4,
        fontSize:11,
    },
    categoryContainer: {
        marginTop: 5,
        marginBottom: 5,
        height: 100,
    },
});
