import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, Text, View, ImageBackground } from 'react-native';
import { connect } from 'react-redux';
import { storeActions } from '../actions/store.action'
import { Ionicons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
// import Applogo from '../../assets/applogo.svg';
// import Award from '../../assets/award.svg';

function Header(props) {
  const [index, setIndex] = useState(0);
  let myCorousel = useRef();
  useEffect(() => {

  });

  return (
    <View style={styles.HeaderContainer}>
      <View style={styles.leftAligned}>
        <View style={styles.backButton}>
          <Ionicons name="ios-arrow-back" size={24} color="black" />
        </View>
        <View style={styles.logo}>
          {/* <Award></Award>  */}
          <ImageBackground
            source={'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVKoKm3FGeOxfMaYeASdg8cOcPxDIzJL2-fI_nrBjVUHXheNdn&usqp=CAU'}
            style={styles.image}
            imageStyle={styles.imageStyle}
          >
          </ImageBackground>
        </View>
      </View>
      <View style={styles.centerArea}>
        <View style={styles.mainHeading}>
          <Text style={styles.title}>Pawan Super Market</Text>
          <View style={styles.subHeading}>
            <Text style={styles.distance}>2.5Km</Text>
            <Text style={styles.subTitle}>Create list and order</Text>
          </View>
        </View>
        <View style={styles.plusButton}>
          <FontAwesome name="plus-circle" size={18} color="black" />
        </View>
        <View style={styles.orderType}>
            <MaterialCommunityIcons onClick={() => props.setView('9by9')} name="view-dashboard" size={20} color={props.currentViewType=='9by9'? 'darkorange': 'burlywood'} />
            <MaterialCommunityIcons onClick={() => props.setView('list')} name="view-list" size={20} color={props.currentViewType=='list'? 'darkorange': 'burlywood'} />
        </View>
      </View>
      <View style={styles.searchButton}>
        <Ionicons name="ios-search" size={24} color="white" />
      </View>
    </View>

  );
}

const styles = StyleSheet.create({
  HeaderContainer: {
    height: 60,
    backgroundColor: 'lightyellow',
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 7,
  },
  backButton: {
    width: 18,

  },
  searchButton: {
    height: 35,
    width: 35,
    borderRadius: 17,
    alignItems: 'center',
    backgroundColor: 'darkorange',
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerArea: {
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
  },
  logo: {
    width: 35,
    height: 35,
  },
  leftAligned: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    flex: 1,
    justifyContent: "center",
    alignSelf: 'stretch',
  },
  imageStyle: {
    resizeMode: "cover",
    alignSelf: 'stretch',
    borderRadius: 17,
  },
  mainHeading: {
    flexDirection: 'column'
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  subHeading: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  distance: {
    fontSize: 10,
    width: 50,
  },
  subTitle: {
    fontSize: 10,
    alignSelf: 'stretch'
  },
  plusButton: {
    marginLeft:7,
    marginRight:7,
  },
  orderType: {
    flexDirection: 'column'
  },
  faintColor: {
    color:'burlywood'
  },
  darkColor: {
    color:'darkorange'
  }
});



const mapStateToProps = (state, props) => {
  const { currentViewType } = state;
  return {
    currentViewType,
  };
}

const mapDispatchToProps = (dispatch) => ({
  setView:(viewType)=>dispatch({type:"SET_VIEW_TYPE",payload:viewType}),
})

export default connect(mapStateToProps, mapDispatchToProps)(Header);