import { AmplifyTheme } from 'aws-amplify-react-native';



const MyAmazonSignInButton = Object.assign({}, AmplifyTheme.amazonSignInButton, {    
    background: 'white',
    marginLeft: '-20px;',
    width: '113%',
    marginTop: '-1px',
    marginBottom: '-1px'
});

const MySectionHeader = Object.assign({}, AmplifyTheme.sectionHeader, {
    color: '#FFFFFF',
    background: 'white',
    borderBottomLeftRadius: '32px',
    borderBottomRightRadius: '32px',
    paddingBottom: '10px',
});
const MyText = Object.assign({}, AmplifyTheme.text, {
    color: '#FFFFFF',
});

const MyActionRow = Object.assign({}, AmplifyTheme.ActionRow, {
    color: '#FFFFFF',
});
const MyContainer = Object.assign({}, AmplifyTheme.container, {
    background: '#203189',
    marginLeft: '-20px',
    marginRight: '-20px',
    textAlign: 'center',
});
const MySignIn = Object.assign({}, AmplifyTheme.signIn, { paddingLeft: '0',  paddingRight: '0' });
// const MyFormSection = Object.assign({}, AmplifyTheme.formSection, { background: '#FFFFFF' });
const MySectionBody = Object.assign({}, AmplifyTheme.sectionBody, {
    background: '#FFFFFF',
    marginLeft: '-20px',
    marginRight: '-20px',
    marginTop: '20px',
    textAlign: 'left',
    padding: '40px',
    borderTopLeftRadius: 32, 
    borderTopRightRadius: 32, 
});
const MyTheme = Object.assign({}, AmplifyTheme, {
    text: MyText, 
    amazonSignInButton: MyAmazonSignInButton, 
    sectionHeader: MySectionHeader, 
    ActionRow: MyActionRow, 
    container: MyContainer, 
    signIn: MySignIn, 
    //  formSection: MyFormSection, 
    sectionBody: MySectionBody, 
});

export default MyTheme;
