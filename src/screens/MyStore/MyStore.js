import React, { useState, useRef, useEffect } from 'react';
import { Dimensions } from 'react-native';
import { StyleSheet, Text, View } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { scrollInterpolator, animatedStyles } from '../../utils/animations';
import Shelf from '../OrderCreation/GridOrder/Shelf.js'
import { connect } from 'react-redux';
import { storeActions } from '../../actions/store.action'
import AddProductToCart from '../OrderCreation/GridOrder/AddProductToCart'
import StoreCategories from './StoreCategories'
import ListOrder from '../OrderCreation/ListOrder/ListOrder'
const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.8);

const DATA = [
  "https://www.kantarretailiq.com/Content/Render/1489183.jpeg",
  "http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/nebula_brown.png",
  "https://previews.123rf.com/images/junpinzon/junpinzon1502/junpinzon150200031/37789861-dishwashing-products-sold-in-a-grocery-store.jpg",
  "http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/nebula_blue.s2014.png",
  "http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/nebula_blue.s2014.png",
];




function _renderItem({ item }) {
  return (
    <View style={styles.itemContainer}>
      <Shelf imageUri={item} itemWidth={ITEM_WIDTH}></Shelf>
    </View>
  );
}


function MyStore(props) {
  const [index, setIndex] = useState(0);
  let myCorousel = useRef();
  useEffect(() => {
    myCorousel.current && myCorousel.current.focus()
    getStoresByPincode(411019);

  });

  const getStoresByPincode = (pincode) => {
    let requestBody = {
      "item": {
        "pincode": pincode,
        "type": "GROCERY"
      }
    };

    props.getStoresByPincode(requestBody, (err, res) => {
      if (err) {
        console.log(err);
      } else {
        console.log(res);
      }
    })
  }




  return (
    <View style={styles.corouselContainer}>
      <View style={styles.corouselHeader}>
        <StoreCategories></StoreCategories>
      </View>
      {
        props.currentViewType == '9by9' ?
          <View>
            <Carousel
              ref={(c) => myCorousel = c}
              data={DATA}
              renderItem={_renderItem}
              sliderWidth={SLIDER_WIDTH}
              itemWidth={ITEM_WIDTH}
              containerCustomStyle={styles.corousel}
              inactiveSlideShift={0}
              onSnapToItem={(index) => setIndex(index)}
              // scrollInterpolator={scrollInterpolator}
              // slideInterpolatedStyle={animatedStyles}
              useScrollView={true}
            />
            <View style={styles.corouselFooter}>
              {
                props.activeProduct && !props.activeProduct.hasOwnProperty('id') ?
                  <Text style={styles.placeOrder}>Tap the product & place the order </Text>
                  :
                  <AddProductToCart></AddProductToCart>
              }
            </View>
          </View>
          : 
            <ListOrder></ListOrder>
        }
    </View>

  );
}

const styles = StyleSheet.create({
  corouselContainer: {
    flex: 1,
    alignSelf: 'stretch',
  },
  corousel: {

  },
  itemContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'dodgerblue'
  },
  itemLabel: {
    color: 'white',
    fontSize: 24
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  corouselHeader: {
  },
  corouselFooter: {
    height: 'auto',
  },
  placeOrder: {
    paddingRight: 7,
    paddingLeft: 7,
    paddingBottom: 10,
    paddingTop: 10,
    textAlign: 'center',
    backgroundColor: '#203189',
    borderWidth: 0,
    color: 'white',
    marginTop: 5,
    marginBottom: 5,
    alignSelf: 'center',
  },

});


const mapStateToProps = (state, props) => {
  const { activeProduct, currentViewType } = state;
  return {
    activeProduct, currentViewType
  };
}

const mapDispatchToProps = (dispatch) => ({
  getStoresByPincode: (queryDetails, callback) => dispatch(storeActions.getStoresByPincode(queryDetails, callback)),
})

export default connect(mapStateToProps, mapDispatchToProps)(MyStore);