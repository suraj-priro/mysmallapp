import React, { useState, useRef, useEffect } from 'react';
import { Dimensions } from 'react-native';
import { StyleSheet, Text, View, ScrollView  } from 'react-native';
import { connect } from 'react-redux';
import { storeActions } from '../../actions/store.action'
import { MaterialCommunityIcons } from '@expo/vector-icons'; 

const categories = [
  {
    color: 'blue',
    category: 'SPECIAL OFFERS',
    icon:true,
  },
  {
    color: 'pink',
    category: 'Grocery',
    icon:false,
  },
  {
    color: 'orange',
    category: 'healthCare',
    icon:false,
  },
  {
    color: 'yellow',
    category: 'Food',
    icon:false,
  },
  {
    color: 'pink',
    category: 'Grocery',
    icon:false,
  },
  {
    color: 'orange',
    category: 'healthCare',
    icon:false,
  },
  {
    color: 'yellow',
    category: 'Food',
    icon:false,
  },
];

function StoreCategories(props) {
    useEffect(() => {
    });

    return (
      <ScrollView  style={styles.categoryContainer} horizontal showsHorizontalScrollIndicator={false}>
        {/* <View style={styles.categoryContainer}> */}
          {
            categories.map((category, index) => 
              <View key={index} style={[styles.category, {backgroundColor:category.color}]}>
                {
                  category.icon?
                  <MaterialCommunityIcons name="brightness-percent" size={16} color="white" />
                  : null
                }
                <Text style={styles.categoryText}>{category.category}</Text>
              </View>
              )
          }
        </ScrollView>
        
    );
  }

  const styles = StyleSheet.create({
    categoryContainer: {
      paddingTop:5,
      paddingBottom:5,
    },
    category: {
     marginLeft:3,
     marginRight:3,
     padding:4,
     paddingLeft:6,
     paddingRight:6,
     display:'flex',
     justifyContent:'flex-start',
     alignItems:'center',
     flexDirection:'row',
     borderRadius: 15,
     shadowColor: '#000',
     shadowOffset: {width: 0, height: 2},
     shadowOpacity: 0.8,
     shadowRadius: 2,
    },
    categoryText: {
      color: 'white',
      lineHeight: 13,
      fontSize:13,
    }
  });
    
const mapDispatchToProps = (dispatch) => ({
  getStoresByPincode:(queryDetails,callback)=> dispatch(storeActions.getStoresByPincode(queryDetails,callback)),
})

export default connect(null,mapDispatchToProps)(StoreCategories);