/*This is an Example of Grid View in React Native*/
import React, { Component } from 'react';
//import rect in our project
import {
  StyleSheet,
  View,
  FlatList,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from 'react-native';
//import all the components we will need
import { connect } from 'react-redux';

class ImageGrid extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      dataSource: {},
      gridHeight: 0,
      sections: [0,1,2,3,4,5,6,7,8],
    };
    this.selectProduct = this.selectProduct.bind(this);
  }
  componentDidMount() {
    console.log(this.props.silderHeight)
    var that = this;
    let items = Array.apply(null, Array(81)).map((v, i) => {
      return { id: i, src: '', selected:false, orderCount: 1};
    });
    that.setState({
      dataSource: items,
    });
  }


  selectProduct(index) {
    this.state.dataSource[index].selected = !this.state.dataSource[index].selected; 
    this.setState({
      dataSource: [...JSON.parse(JSON.stringify(this.state.dataSource))]
    }, () => {
      this.props.updateActiveProduct({
        id: index,
      })
    })
  }
  render() {
    return (
      <View style={styles.MainContainer}>
        {
        this.state.sections.map((row, index) =>
          <View key={index} style={[styles.row, {height:this.props.gridItemHeight }]}>
          {
          this.state.sections.map((col, index) =>
            <View key={index} style={[styles.column, 
                          {width:this.props.gridItemWidth},
                          col%3==0 && row%3==0? styles.bothBig: col%3==0? styles.leftBig: row%3==0? styles.topBig: styles.smallB,
                          this.state.dataSource && this.state.dataSource[row*8 + (row+col)] && this.state.dataSource[row*8 + (row+col)].selected? styles.grayBackground: styles.transparentBackground,
                        ]}
              onClick={() =>{this.selectProduct(row*8 + (row+col))}}
            >
              {/* { row*8 + (row+col)} */}
              {
                this.state.dataSource && this.state.dataSource[row*8 + (row+col)] && this.state.dataSource[row*8 + (row+col)].selected?
                this.state.dataSource[row*8 + (row+col)].orderCount
                : null
              }
            </View>
          )
          }
        </View>
        )
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    alignSelf:'stretch',
  },
  grayBackground: {
    backgroundColor: 'lightgray',
    color: 'black',
  },
  transparentBackground: {
    backgroundColor: 'transparent'
  },
  bothBig: {
    borderStyle:"solid",
    borderColor:"white",
    borderLeftWidth: 2,
    borderTopWidth: 2,
    backgroundColor: 'red',
  },
  leftBig: {
    borderStyle:"solid",
    borderColor:"white",
    borderTopWidth: 1,
    borderLeftWidth: 2,
    backgroundColor: 'red',
  },
  topBig: {
    borderStyle:"solid",
    borderColor:"white",
    borderTopWidth: 2,
    borderLeftWidth:1,
    backgroundColor: 'red',
  },

  smallB: {
    borderLeftWidth: 1,
    borderRightWidth: 0,
    borderTopWidth: 1,
    borderBottomWidth: 0,
    borderStyle:"solid",
    borderColor:"white",
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'center',
    
  },
  column: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = (state, props) => {
  const { currentOrder} = state;
  return {
    currentOrder,
  };
}


// Mapping dispatch functions to props to access easily
const mapDispatchToProps = (dispatch) => ({
  updateCurrentOrder:(currentOrder)=>dispatch({type:"UPDATE_CURRENT_ORDER",payload:currentOrder}),
  updateActiveProduct:(ActiveProduct)=>dispatch({type:"UPDATE_ACTIVE_PRODUCT",payload:ActiveProduct}),
  resetCurrentOrder:(currentOrder)=>dispatch({type:"RESET_CURRENT_ORDER",payload:currentOrder}),
})

export default connect(mapStateToProps, mapDispatchToProps)(ImageGrid);