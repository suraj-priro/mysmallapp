import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, Text, View, ImageBackground, Image } from 'react-native';
import ImageOverlay from "react-native-image-overlay";
import ImageGrid from './ImageGrid.js'

function imageDimensions(uri) {
    return new Promise((resolve, reject) => {
        Image.getSize(uri, (width, height) => {
            resolve({ width: width, height: height });
        }, (error) => { reject(error) });
    });
}

export default function Shelf(props) {

    const [gridItemWidth, setGridItemWidth] =  useState(0);
    const [gridItemHeight, setGridItemHeight] =  useState(0);
    
    useEffect(() => {
        imageDimensions(props.imageUri).then(response => {
            if (response.height) {
                let gridItemWidth = props.itemWidth/9;
                setGridItemWidth(gridItemWidth);
                let newImageHeight = props.itemWidth/response.width*response.height; 
                let gridItemHeight =  newImageHeight/9;
                setGridItemHeight(gridItemHeight);
            }
        });
    });

    return (
        <ImageBackground
            source={props.imageUri}
            style={styles.image}
            imageStyle={styles.imageStyle}
        >
            <View style={styles.gridContainer}>
                <ImageGrid gridItemHeight={gridItemHeight} gridItemWidth={gridItemWidth}></ImageGrid>
            </View>
        </ImageBackground>

        // <ImageOverlay
        // source={{ uri: props.imageUri}}
        // containerStyle={styles.image}
        // width={'100%'}
        // height={'100%'}
        // contentPosition="top">

        // </ImageOverlay>
    )
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        justifyContent: "center",
        alignSelf: 'stretch',
    },
    imageStyle: {
        resizeMode: "contain",
        alignSelf: 'stretch',
    },
    gridContainer: {
        flex: 1,
        alignSelf: 'stretch',
    }
});
