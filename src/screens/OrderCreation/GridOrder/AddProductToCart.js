import React, { useState, useRef, useEffect } from 'react';
import { Dimensions } from 'react-native';
import { StyleSheet, Text, View } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { connect } from 'react-redux';
import { storeActions } from '../../../actions/store.action'
import {Picker} from '@react-native-community/picker';
import { FontAwesome } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

function AddProductToCart(props) {
    const [selectedUnit, setSelectedUnit] = useState('units');
    const [units, setUnits] = useState(0);
    useEffect(() => {
    
    });

    const showQuestionInput = () => {

    }
    const addProductToCart = () => {

    }
    return (
        <View style={[styles.unitsContainer, styles.evenlyFlex,]}>
            <View>             
                <Picker
                    selectedValue={selectedUnit}
                    style={[styles.picker, styles.evenlyFlex, styles.button]}
                    onValueChange={(itemValue, itemIndex) => setSelectedUnit(itemValue)}
                >
                    <Picker.Item label="Units" value="units" />
                    <Picker.Item label="Kg" value="kg" />
                    <Picker.Item label="Grams" value="grams" />
                </Picker>
            </View>
            <View style={[styles.evenlyFlex, {width: 105} ]}>
                <FontAwesome name="minus-square-o" size={24} color="#203189" onClick={() => {units-1 >=0 && setUnits(units-1)}} />
                <Text>{units}</Text>
                <FontAwesome name="plus-square-o" size={24} color="#203189" onClick={() => {setUnits(units+1)}} />
            </View>
            <View style={[styles.evenlyFlex, ]}>
                <Text style={[styles.button, {fontSize: 12, paddingLeft:9, paddingRight:9}]} onClick={addProductToCart()}>Add</Text>
            </View>
            <View style={[styles.evenlyFlex ]}>
                <MaterialCommunityIcons  onClick={showQuestionInput()} name="comment-question" size={24} color="#203189" />
            </View>

        </View>
    );
  }

  const styles = StyleSheet.create({
    unitsContainer: {
      marginTop:5,
      marginBottom:5,
      
    },
    picker: {
        width: 80,
    },
    evenlyFlex: {
        display:'flex',
        flexDirection:'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    button: {
        borderRadius: 4,
        borderWidth: 1,
        borderStyle:'solid',
        borderColor: '#203189',
        padding: 3,
    },
    
  });
    
const mapDispatchToProps = (dispatch) => ({
  getStoresByPincode:(queryDetails,callback)=> dispatch(storeActions.getStoresByPincode(queryDetails,callback)),
})

export default connect(null,mapDispatchToProps)(AddProductToCart);