import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, Text, View, ScrollView,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import { FontAwesome } from '@expo/vector-icons';
import OrderItem from './OrderItem'
import { AntDesign } from '@expo/vector-icons';

function ListOrder(props) {
  let [orderList, setOrderList] = useState([
    {
      productInfo: 'Balaji Chips',
      productQuantity: '3 packets',
    },
    {
      productInfo: 'Chitale Milk',
      productQuantity: '1 litr',
    },
    {
      productInfo: 'Dairy Milk of 10rs',
      productQuantity: '3',
    }

  ])
  useEffect(() => {

  });

  const addNewItem = () => {
    setOrderList([...orderList, {
      productInfo: '',
      productQuantity: '',
    }])
  }

  return (
    <View style={[styles.unitsContainer]}>
      <View style={styles.header}>
        <View style={styles.information}>
          <Text style={styles.info}>
            Type or audio record your items below.
          </Text>
          <Text style={styles.info}>
           You can also upload a picture of written note.
          </Text>
        </View>

        <View style={styles.icons}>
        <FontAwesome style={styles.icon} name="camera" size={18} color="black" />
        <FontAwesome style={styles.icon} name="microphone" size={18} color="black" />
        </View>
      </View>

      <ScrollView style={styles.content} showsVerticalScrollIndicator={false}>
        {
          orderList.map((order, index) =>
            <OrderItem order={order} key={index} index={index}></OrderItem>
          )
        }
      </ScrollView>

      <View style={styles.footer}>

      </View>
      <TouchableOpacity
          activeOpacity={0.7}
          onPress={addNewItem}
          style={styles.TouchableOpacityStyle}>
            <AntDesign name="plus" size={18} color="white" />
        </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  unitsContainer: {
    flex: 1,
  },
  evenlyFlex: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  header: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 6,
    paddingRight: 6,
    alignSelf: 'stretch',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  information: {
    alignSelf: 'stretch',
    flexDirection: 'column',
    flex:4,
  },
  info: {
    fontSize: 12,
  },
  icons: {
    flexDirection:'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flex:1,
  },
  icon: {
    marginLeft: 2,
    marginRight: 2,
  },
  TouchableOpacityStyle: {
    position: 'absolute',
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    right: 10,
    bottom: 10,
    backgroundColor: 'darkorange',
    borderRadius: 15,
  },
});

const mapDispatchToProps = (dispatch) => ({
})

export default connect(null, mapDispatchToProps)(ListOrder);