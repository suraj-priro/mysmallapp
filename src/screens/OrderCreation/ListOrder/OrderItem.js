import React, { useState, useRef, useEffect } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Animated   } from 'react-native';
import { connect } from 'react-redux';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { FontAwesome } from '@expo/vector-icons';

function OrderItem(props) {
  useEffect(() => {

  });

  const onChangeText = (text) => {

  }

  const deleteOrderItem = (index) => {
    
  }

  const renderRightActions = (progress, dragX, index) => {
    const scale = dragX.interpolate({
      inputRange: [-70, 0],
      outputRange: [1, 0],
      extrapolate: 'clamp',
    })
    return (
      <View style={styles.actionContainer}>
        <Animated.View
          style={{
            color: 'white',
            paddingHorizontal: 10,
            transform: [{ scale }]
          }}>
          <FontAwesome name="trash-o" size={30} color="white" onClick={() => {deleteOrderItem(index)}}/>
        </Animated.View>
      </View>
    );
  };

  return (
    <Swipeable
      renderRightActions={(progress, dragX)=>renderRightActions(progress, dragX, props.index)}>
    <View style={[styles.orderItem]}>
        <View style={styles.leftAligned}>
            <Text style={styles.index}>{props.index + 1}.</Text>
            <TextInput
                style={styles.input}
                onChangeText={text => onChangeText(text)}
                value={props.order.productInfo}
            />
        </View>
        <View style={styles.rightAligned}>
            <TextInput
                style={styles.input}
                onChangeText={text => onChangeText(text)}
                value={props.order.productQuantity}
            />
        </View>
    </View>
    </Swipeable>
  );
}

const styles = StyleSheet.create({
  orderItem: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    paddingLeft:10,
    paddingRight:10,
    paddingTop:6,
    paddingBottom:6,
    borderBottomColor: 'lightgray',
    borderStyle: 'solid',
    borderBottomWidth: 1,
    justifyContent: 'space-between'
  },
  leftAligned: {
      flexDirection: 'row',
      flex: 3,
      alignItems: 'center',
  },
  rightAligned: {
      flexDirection: 'row',
      flex: 1,
      alignItems: 'center',
  },
  index: {
      fontWeight: 'bold',
      marginRight: 5,
  },
  input: {
      padding: 6,
  },
  actionContainer: {
    justifyContent: 'center',
    backgroundColor: 'darkorange'
  }
});

const mapDispatchToProps = (dispatch) => ({
})

export default connect(null, mapDispatchToProps)(OrderItem);