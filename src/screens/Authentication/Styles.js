import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#203189',
        flex:1,
        justifyContent: 'space-between',
        paddingTop:0,
        marginTop:-20,
        alignItems: 'center',
        alignSelf: 'stretch',
    },
    formContainer: {
        backgroundColor: '#FFFFFF',
        marginTop: 20,
        padding: 40,
        borderTopLeftRadius: 32, 
        borderTopRightRadius: 32,
        paddingTop: 10,
        alignSelf: 'stretch',
    },
    screenHeading : {
        alignItems: 'center',       //centers the View's children on the y-axis
        justifyContent: 'center',
        paddingTop: 16,
    },
    screenHeadingText: {
        color: '#FFFFFF',
        fontSize: 16,
    },
    blueHeading: {
        color: '#203189',
        fontSize: 20,
        fontWeight: '600',  
    },
    inputM: {
        marginTop: 10,
        marginBottom: 10,
    },
    textInput: {
        paddingLeft: 5,
        borderWidth: 1,
        height: 35,
        borderColor: 'gray',
        borderStyle: "solid",
    },
    inlineFlex: {
        display: 'flex',
        //flexDirection: 'initial',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inlineFlexEven: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
    },
    dash: {
        width: 15,
        height: 1,
        backgroundColor:'darkslategray'
    },
    circle: {
        width: 25,
        height: 25,
        borderRadius: 12,
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'gray',
        lineHeight: 22,
        fontSize: 20,
        textAlign: 'center',
        marginLeft: 3,
        marginRight: 3,
    },
    rightTxt: {
        alignItems:'flex-end',
    },
    centerTxt: {
        alignItems:'center',
    },
    ornTxt: {
        color: '#FF7F3E',
        fontSize: 11,
    },
    hide: {
        display: 'none',
    },
    phone_container: {
        flex: 1,
        alignItems: "center",
        padding: 20,
        paddingTop: 60
      },
      phone_info: {
        // width: 200,
        borderRadius: 5,
        backgroundColor: "#f0f0f0",
        padding: 10,
        marginTop: 20
      },
      phone_button: {
        marginTop: 20,
        padding: 10
      },
      codeItem: {
        borderBottomWidth: 1,
        borderStyle: 'solid',
        borderColor:'gray',
        width: 33,
        height: 33,
        textAlign: 'center',
      }      
});

export default styles;
