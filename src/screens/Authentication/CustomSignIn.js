import React, { Component } from "react";
import {Text, View, Button, TextInput, SafeAreaView,
  Keyboard,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Modal,
  FlatList,
  TouchableOpacity,
  TouchableHighlight,
} from 'react-native';
import { Auth } from "aws-amplify";
import styles from './Styles'
import { SignIn } from 'aws-amplify-react-native';
import common from '../../styles/commonStyles'
import phoneInputStyle from '../../styles/phoneInput'
import CountryCodeModal from './CountryCodeModal'

import {
  Container,
  Item,
  Input,
  Icon,
  
} from 'native-base'
import data from './CountryCodes'
// import {SvgUri} from 'react-native-svg-uri';


// Default render of country flag
const defaultFlag = data.filter(
  obj => obj.name === 'United Kingdom'
  )[0].flag

export default class CustomSignIn extends SignIn {
  constructor(props) {
    super(props);
    this._validAuthStates = ["signIn", "signedOut", "signedUp"];
    this.signIn = this.signIn.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleFormSubmission = this.handleFormSubmission.bind(this);
    this.showForgotPassword = this.showForgotPassword.bind(this);
    this.showSignUp = this.showSignUp.bind(this);
    this.showConfirmCode = this.showConfirmCode.bind(this);
    this.state = {

      flag: defaultFlag,
      modalVisible: false,
      phoneNumber: '',
    };
  }

  handleFormSubmission(evt) {
    evt.preventDefault();
    this.signIn();
  }
  
  showForgotPassword() {
    this.props.onStateChange('forgotPassword', {}) 
  }

  showSignUp() {
    this.props.onStateChange('signUp', {}) 
  }

  showConfirmCode() {
    this.props.onStateChange('confirmSignUp', {})
  }

  async signIn() {
    if (this.inputs) {
      const username = this.inputs.username;
      const password = this.inputs.password;
      try {
        this.props.onStateChange('loading', {})
        await Auth.signIn(username, password);
        this.props.onStateChange("signedIn", {});
      } catch (err) {
        if (err.code === "UserNotConfirmedException") {
          this.props.updateUsername(username);
          await Auth.resendSignUp(username);
          this.props.onStateChange("confirmSignUp", {});
        } else if (err.code === "NotAuthorizedException") {
          // The error happens when the incorrect password is provided
          this.setState({ error: "Login failed." });
        } else if (err.code === "UserNotFoundException") {
          // The error happens when the supplied username/email does not exist in the Cognito user pool
          this.setState({ error: "Login failed." });
        } else {
          this.setState({ error: "An error has occurred." });
          console.error(err);
        }
      }
    }

  }

  handleInputChange(evt) {
    this.inputs = this.inputs || {};
    const { name, value, type, checked } = evt.target;
    const check_type = ["radio", "checkbox"].includes(type);
    this.inputs[name] = check_type ? checked : value;
    this.inputs["checkedValue"] = check_type ? value : null;
    this.setState({ error: "" });
  }

  showModal() {
    this.setState({ modalVisible: true })
  }
  hideModal() {
    this.setState({ modalVisible: false })
     // Refocus on the Input field after selecting the country code
    this.refs.PhoneInput._root.focus()
  }
  async selectCountry(country) {
    // Get data from Countries.js  
    const countryData = await data
    try {
      // Get the country code
      const countryCode = await countryData.filter(
        obj => obj.name === country
      )[0].dial_code
      // Get the country flag
      const countryFlag = await countryData.filter(
        obj => obj.name === country
      )[0].flag
      // Update the state then hide the Modal
      this.setState({ phoneNumber: countryCode, flag: countryFlag })
      await this.hideModal()
    }
    catch (err) {
      console.log(err)
    }
  }
  onChangeText(key, value) {
    this.setState({
      [key]: value
    })
  }
  render() {
    const countryData = data
    return (
      <SafeAreaView  style={[this._validAuthStates.includes(this.props.authState) ? styles.container : styles.hide,]}>
        <View style={styles.screenHeading}>
          {/* <SvgUri width="200" height="200" source={require('../../../assets/9x9_logo_sm.svg')} /> */}
          <Text style={styles.screenHeadingText}>9by9</Text>
        </View>
        <View style={styles.formContainer}>
        <View>
          {this._validAuthStates.includes(this.props.authState) && (
            <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={this.handleFormSubmission}>
               <View style={styles.screenHeading}>
                <Text style={styles.blueHeading}>Login</Text>
              </View>
              <View style={{...styles.inputM, marginTop:'30px'}}>
                <Text style={common.label}>Username</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Enter mobile number"
                  name="username"
                  keyboardType="phone-pad"
                  onChange={this.handleInputChange} 
                />
              </View>
              {/* <View style={styles.inputM}>
                <Item style={phoneInputStyle.itemStyle}>
                  <Icon
                    name='call'
                    style={phoneInputStyle.iconStyle}
                  />
                   <View><Text>{this.state.flag}</Text></View>
                  <Icon
                      name='md-arrow-dropdown'
                      style={[styles.iconStyle, { marginLeft: 0 }]}
                      onPress={() => this.showModal()}
                    />
                    <Input
                      placeholder='+44766554433'
                      placeholderTextColor='#adb4bc'
                      keyboardType={'phone-pad'}
                      returnKeyType='done'
                      autoCapitalize='none'
                      autoCorrect={false}
                      secureTextEntry={false}
                      style={styles.inputStyle}
                      value={this.state.phoneNumber}
                      ref='PhoneInput'
                      onChangeText={(val) => this.onChangeText('phoneNumber', val)}
                    />
                </Item>
                </View> */}

              <View style={styles.inputM}>
                <Text style={common.label}>Password</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Password"
                  name="password"
                  textContentType="password"
                  keyboardType="default"
                  secureTextEntry={true}
                  onChange={this.handleInputChange} 
                />
              </View>
              <View style={[styles.rightTxt]}>
                <Text style={styles.ornTxt} onClick={this.showForgotPassword}>Forgot Password</Text>
              </View> 
              
              <View style={styles.inputM}>
                <Button
                  onPress={this.handleFormSubmission}
                  title="Sign In"
                  color="#203189"
                  accessibilityLabel="Sign In"
                />
              </View>
              <View style={[styles.inlineFlexEven, {...styles.inputM, marginTop:'30px'}]}>
                <Text style={styles.ornTxt} onClick={this.showSignUp}>New user? Register</Text>
                <Text style={styles.ornTxt} onClick={this.showConfirmCode}>Confirm Code</Text>
              </View> 
            </form>
          )}
        </View>
        </View>    
      </SafeAreaView>
    );
  }
}