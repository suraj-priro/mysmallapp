import React, { Component } from "react";
import { Text, View, Button, TextInput, SafeAreaView } from 'react-native';
import { Auth } from "aws-amplify";
import styles from './Styles'
import { ForgotPassword } from 'aws-amplify-react-native';
export default class CustomForgotPassword extends ForgotPassword {
    constructor(props) {
        super(props);
        this._validAuthStates = ["forgotPassword", "forgotPasswordSubmit"];
        this.forgotPassword = this.forgotPassword.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmission = this.handleFormSubmission.bind(this);
        this.showSignIn = this.showSignIn.bind(this);
        this.state = {};
    }

    handleFormSubmission(evt) {
        evt.preventDefault();
        if (this.props.authState == "forgotPassword") {
            this.forgotPassword();
        } else if (this.props.authState == "forgotPasswordSubmit") {
            this.submitPassword();
        }
    }

    showSignIn() {
        this.props.onStateChange('signIn', {})
    }
    async forgotPassword() {
        if (this.inputs) {
            const username = this.inputs.username;
            try {
                this.props.onStateChange('loading', {})
                await Auth.forgotPassword(username);
                this.props.onStateChange("forgotPasswordSubmit", {});
            } catch (err) {
            }
        }

    }

    async submitPassword() {
        if (this.inputs) {
            const username = this.inputs.username;
            const password = this.inputs.password;
            const code = this.inputs.code;
            try {
                this.props.onStateChange('loading', {})
                await Auth.forgotPasswordSubmit(username, code, password);
                this.props.onStateChange("signIn", {});
            } catch (err) {
            }
        }

    }

    handleInputChange(evt) {
        this.inputs = this.inputs || {};
        const { name, value, type, checked } = evt.target;
        const check_type = ["radio", "checkbox"].includes(type);
        this.inputs[name] = check_type ? checked : value;
        this.inputs["checkedValue"] = check_type ? value : null;
        this.setState({ error: "" });
    }

    render() {
        return (
            <SafeAreaView style={[this._validAuthStates.includes(this.props.authState) ? styles.container : styles.hide,]}>
                
                <View style={styles.screenHeading}>
                    <Text style={styles.screenHeadingText}>9by9</Text>
                </View>
                <View style={styles.formContainer}>
                    <View>
                        {this._validAuthStates.includes(this.props.authState) && (
                            <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={this.handleFormSubmission}>
                                <View style={styles.screenHeading}>
                                    <Text style={styles.blueHeading}>Reset Your Password</Text>
                                </View>

                                <View  style={{...styles.inputM, marginTop:'30px'}}>
                                    <TextInput
                                        style={styles.textInput}
                                        placeholder="Username"
                                        name="username"
                                        keyboardType="default"
                                        onChange={this.handleInputChange}
                                    />
                                </View>
                                {
                                 this.props.authState == "forgotPassword"?
                                    <View style={styles.inputM}>
                                        <Button
                                            onPress={this.handleFormSubmission}
                                            title="Send Code"
                                            color="#203189"
                                            accessibilityLabel="Send Code"
                                        />
                                    </View>
                                 : null   
                                }
                                {
                                    this.props.authState == "forgotPasswordSubmit"?
                                    <View style={styles.inputM}>
                                        <TextInput
                                            style={styles.textInput}
                                            placeholder="code"
                                            name="code"
                                            keyboardType="default"
                                            onChange={this.handleInputChange}
                                        />
                                    </View>
                                 : null    
                                }
                                {
                                    this.props.authState == "forgotPasswordSubmit"?
                                    <View style={styles.inputM}>
                                        <TextInput
                                        style={styles.textInput}
                                        placeholder="Password"
                                        name="password"
                                        textContentType="password"
                                        autoCompleteType="password"
                                        keyboardType="default"
                                        secureTextEntry={true}
                                        onChange={this.handleInputChange} 
                                        />
                                    </View>
                                 : null    
                                }
                                {
                                 this.props.authState == "forgotPasswordSubmit"?
                                    <View style={styles.inputM}>
                                        <Button
                                            onPress={this.handleFormSubmission}
                                            title="Submit Password"
                                            color="#203189"
                                            accessibilityLabel="Submit Password"
                                        />
                                    </View>
                                 : null   
                                }
                                <View style={[{ ...styles.inputM, }, styles.centerTxt]}>
                                    <Text style={styles.ornTxt} onClick={this.showSignIn}>Back To Sign In</Text>
                                </View>
                            </form>
                        )}
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}