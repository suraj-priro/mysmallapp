import React, { Component } from "react";
import { Text, View, ActivityIndicator, } from 'react-native';
import styles from './Styles'
import common from '../../styles/commonStyles'

export default class CustomLoading extends Component {
    constructor(props) {
        super(props);
        this._validAuthStates = ["loading"];
        this.state = {};
    }


    render() {
        return (
            <View style={[this._validAuthStates.includes(this.props.authState) ? common.loaderContainer : styles.hide,]}>
                <ActivityIndicator size="large" color="#0000ff" />
            </View>
        );
    }
}