import React, { Component } from "react";
import { Text, View, Button, TextInput } from 'react-native';
import { Auth } from "aws-amplify";
import styles from './Styles'
import common from '../../styles/commonStyles'
import { SignIn } from 'aws-amplify-react-native';

export default class CustomSignUp extends SignIn {
    constructor(props) {
        super(props);
        this._validAuthStates = ["confirmSignUp"];
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmission = this.handleFormSubmission.bind(this);
        this.showForgotPassword = this.showForgotPassword.bind(this);
        this.showSignIn = this.showSignIn.bind(this);
        this.showSignUp = this.showSignUp.bind(this);
        this.handleCodeChange = this.handleCodeChange.bind(this);
        this.state = {
            username: null,
            codeCharacters: [undefined, undefined, undefined, undefined, undefined, undefined,],
        };
        this.inputRefs=[]
    }

    handleFormSubmission(evt) {
        evt.preventDefault();
        this.confirmSignUp();
    }

    showForgotPassword() {
        this.props.onStateChange('forgotPassword', {})
    }

    showSignIn() {
        this.props.onStateChange('signIn', {})
    }
    showSignUp() {
        this.props.onStateChange('signUp', {})
    }

    
    async confirmSignUp() {
        if (this.inputs) {
            const username = this.inputs.username;
            const code = this.state.codeCharacters.join('');
            console.log("codeCharacters", this.state.codeCharacters)
            console.log("code", code)
            try {
                this.props.onStateChange('loading', {})
                await Auth.confirmSignUp(username, code);
                this.props.onStateChange("signIn", {})
            } catch (error) {
                this.props.onStateChange('confirmSignUp', {})
                console.log('error confirming sign up', error);
            }
        }
    }


    handleInputChange(evt) {
        this.inputs = this.inputs || {};
        const { name, value, type, checked } = evt.target;
        const check_type = ["radio", "checkbox"].includes(type);
        this.inputs[name] = check_type ? checked : value;
        this.inputs["checkedValue"] = check_type ? value : null;
        this.setState({ error: "" });
    }

    handleCodeChange(event, index) {
        this.state.codeCharacters[index] = event.target.value;
        this.setState({codeCharacters: this.state.codeCharacters})
    }
    render() {
        return (
            <View style={[this._validAuthStates.includes(this.props.authState) ? styles.container : styles.hide,]}>
                <View style={styles.screenHeading}>
                    <Text style={styles.screenHeadingText}>9by9</Text>
                </View>
                <View style={styles.formContainer}>
                    <View>
                        {this._validAuthStates.includes(this.props.authState) && (
                            <form onSubmit={this.handleFormSubmission}>
                                <View style={styles.screenHeading}>
                                    <Text style={styles.blueHeading}>Confirm Code</Text>
                                </View>
                                <View style={styles.screenHeading}>
                                    <Text>Enter the 6 digit code sent to you on mail/mobile </Text>
                                </View>
                                <View  style={{...styles.inputM, marginTop:'30px'}}>
                                    <Text style={common.label}>Username</Text>
                                    <TextInput
                                    style={styles.textInput}
                                    placeholder="Enter mobile number"
                                    name="username"
                                    keyboardType="phone-pad"
                                    onChange={this.handleInputChange} 
                                    />
                                </View>
                                <View style={[styles.inlineFlexEven, styles.inputM]}>
                                    {
                                        this.state.codeCharacters.map((item, index) =>
                                        <TextInput
                                            key={'i'+index}
                                            ref={(input) => { this.inputRefs[index] = input; }}
                                            onSubmitEditing={() => { this.inputRefs[index+1] && this.inputRefs[index+1].focus(); }}
                                            style={styles.codeItem}
                                            placeholder="#"
                                            name="code"
                                            keyboardType="default"
                                            onChange={(event) => { this.handleCodeChange(event, index); this.inputRefs[index+1] && this.inputRefs[index+1].focus(); }}
                                            blurOnSubmit={false}
                                            maxLength = {1}
                                        />
                                        )
                                    }
                                </View>
                                <View style={styles.inputM}>
                                    <Button
                                    onPress={this.handleFormSubmission}
                                    title="Submit Code"
                                    color="#203189"
                                    accessibilityLabel="Submit Code"
                                    />
                                </View>
                                <View style={[{ ...styles.inputM, }, styles.centerTxt]}>
                                    <Text style={styles.ornTxt} onClick={this.showSignUp}>Back To Sign Up</Text>
                                </View>
                            </form>
                        )}
                    </View>

                </View>
            </View>
        );
    }
}