import React, { Component } from "react";
import { Text, View, Button, TextInput } from 'react-native';
import { Auth } from "aws-amplify";
import styles from './Styles'
import common from '../../styles/commonStyles'
import { SignIn } from 'aws-amplify-react-native';

export default class CustomSignUp extends SignIn {
    constructor(props) {
        super(props);
        this._validAuthStates = ["signUp"];
        this.signUp = this.signUp.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFormSubmission = this.handleFormSubmission.bind(this);
        this.showForgotPassword = this.showForgotPassword.bind(this);
        this.showConfirmCode = this.showConfirmCode.bind(this);
        this.showSignIn = this.showSignIn.bind(this);
        this.state = {
            username: null
        };
    }

    handleFormSubmission(evt) {
        evt.preventDefault();
        this.signUp();
    }

    showForgotPassword() {
        this.props.onStateChange('forgotPassword', {})
    }

    showConfirmCode() {
        this.props.onStateChange('confirmSignUp', {})
    }

    showSignIn() {
        this.props.onStateChange('signIn', {})
    }

    async signUp() {
        if (this.inputs) {
            const username = this.inputs.username;
            const password = this.inputs.password;
            const phone_number = this.inputs.username;
            const email = this.inputs.email;
            const zip = this.inputs.zip;
            try {
                this.props.onStateChange('loading', {})
                const user = await Auth.signUp({
                    username,
                    password,
                    attributes: {
                        email,
                        phone_number,
                        'custom:zip': zip,
                    }
                });
                console.log({ user });
                this.setState({ username });
                this.props.onStateChange("confirmSignUp", {});
            } catch (err) {
                if (err.code === "UserNotConfirmedException") {
                    this.props.updateUsername(username);
                    await Auth.resendSignUp(username);
                    this.props.onStateChange("confirmSignUp", {});
                } else if (err.code === "NotAuthorizedException") {
                    // The error happens when the incorrect password is provided
                    this.setState({ error: "Login failed." });
                } else if (err.code === "UserNotFoundException") {
                    // The error happens when the supplied username/email does not exist in the Cognito user pool
                    this.setState({ error: "Login failed." });
                } else {
                    this.setState({ error: "An error has occurred." });
                    console.error(err);
                }
            }
        }

    }


    handleInputChange(evt) {
        this.inputs = this.inputs || {};
        const { name, value, type, checked } = evt.target;
        const check_type = ["radio", "checkbox"].includes(type);
        this.inputs[name] = check_type ? checked : value;
        this.inputs["checkedValue"] = check_type ? value : null;
        this.setState({ error: "" });
    }

    render() {
        return (
            <View style={[this._validAuthStates.includes(this.props.authState) ? styles.container : styles.hide,]}>
                <View style={styles.screenHeading}>
                    <Text style={styles.screenHeadingText}>9By9</Text>
                </View>
                <View style={styles.formContainer}>
                    <View>
                        {this._validAuthStates.includes(this.props.authState) && (
                            <form onSubmit={this.handleFormSubmission}>
                                <View style={styles.screenHeading}>
                                    <Text style={styles.blueHeading}>Register</Text>
                                </View>
                                <View style={styles.inputM}>
                                    <Text style={common.label}>Username</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        placeholder="Enter mobile number"
                                        name="username"
                                        keyboardType="phone-pad"
                                        onChange={this.handleInputChange}
                                    />
                                </View>
                                <View style={styles.inputM}>
                                    <Text style={common.label}>Password</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        placeholder="Password"
                                        name="password"
                                        textContentType="password"
                                        keyboardType="default"
                                        secureTextEntry={true}
                                        onChange={this.handleInputChange}
                                    />
                                </View>
                                <View style={styles.inputM}>
                                    <Text style={common.label}>Email Address</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        placeholder="Email"
                                        name="email"
                                        keyboardType="email-address"
                                        onChange={this.handleInputChange}
                                    />
                                </View>
                                <View style={styles.inputM}>
                                    <Text style={common.label}>Zip</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        placeholder="Zip"
                                        name="zip"
                                        keyboardType="phone-pad"
                                        onChange={this.handleInputChange}
                                    />
                                </View>
                                <View style={{ ...styles.inputM }}>
                                    <Button
                                        onPress={this.handleFormSubmission}
                                        title="Sign Up"
                                        color="#203189"
                                        accessibilityLabel="Sign Up"
                                    />
                                </View>
                              
                                <View style={[styles.inlineFlexEven, { ...styles.inputM, marginTop: '30px' }]}>
                                    <Text style={styles.ornTxt} onClick={this.showSignIn}>Already user? Sign-In</Text>
                                    <Text style={styles.ornTxt} onClick={this.showConfirmCode}>Confirm Code</Text>
                                </View>
                            </form>
                        )}
                    </View>
                </View>
            </View>
        );
    }
}