/**
* Copyright (c) 2018-2019 PKP Technologies, LLC.  All Rights Reserved.
* 
* This product and related documentation is protected by copyright and
* distributed under licenses restricting its use, copying, distribution and
* decompilation. No part of this product or related documentation may be
* reproduced in any form by any means without prior written authorization
* from PKP Technologies, LLC and/or its licensors.
*/

import { commonConstants } from '../reducers/constants';
import { storeService  } from '../services/store.service';
import { Storage } from 'aws-amplify';
import axios from 'axios';

export const storeActions={
    getStoresByPincode,

}

function getFileUrl(key,callback)
{
    return dispatch =>{
        Storage.get(key, { level: 'public' }).then(res=>{
            callback(res);   
        }).catch(err => {
            callback("error");
        }); 
    };
}


function getStoresByPincode(options, callback){
    return dispatch => {
    dispatch(showLoader());
    storeService.getStoresByPincode(options).then(res => {
        if (res.success) {
            dispatch(hideLoader());
            callback(null,res);
        }else{
            dispatch(hideLoader());
            callback(null,res);
        }
    }).catch(err => {
        console.log(err);
        dispatch(hideLoader());
        callback(err,null);
    })
};
}


function showLoaderAction(option, callback) {
    return dispatch => {
        dispatch(showLoader())
    }
}

function hideLoaderAction(option, callback) {
    return dispatch => {
        dispatch(hideLoader())
    }
}

function showLoader() {
    return { type: commonConstants.SHOW_LOADER };
}

function hideLoader() {
    return { type: commonConstants.HIDE_LOADER };
}
