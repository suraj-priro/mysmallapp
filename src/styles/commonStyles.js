import { StyleSheet } from 'react-native';

const common = StyleSheet.create({
    loaderContainer: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: 'rgba(0,0,0, 0.5)',
        justifyContent: 'center', alignItems: 'center',
        marginTop: -20,
    },
    label: {
        paddingTop: 10,
        paddingLeft : 0,
        paddingBottom : 5,
        paddingRight : 0,
    },
});


export default common;