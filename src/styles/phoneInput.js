import { StyleSheet } from 'react-native';
const phoneInputStyle = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#aa73b7',
      justifyContent: 'center',
      flexDirection: 'column'
    },
    infoContainer: {
      
    },
    iconStyle: {
      color: '#5a52a5',
      fontSize: 28,
      marginLeft: 15
    },
    itemStyle: {
      marginBottom: 10,
    },
    inputStyle: {
      fontSize: 17,
      fontWeight: 'bold',
      color: '#5a52a5',
    },
  })

export default phoneInputStyle;