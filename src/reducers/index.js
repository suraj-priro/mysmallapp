/**
* Copyright (c) 2018-2019 PKP Technologies, LLC.  All Rights Reserved.
* 
* This product and related documentation is protected by copyright and
* distributed under licenses restricting its use, copying, distribution and
* decompilation. No part of this product or related documentation may be
* reproduced in any form by any means without prior written authorization
* from PKP Technologies, LLC and/or its licensors.
*/

import { combineReducers } from 'redux';
import loader from './loader.reducer';
import {activeProduct, currentOrder, currentViewType} from './store.reducer';
const rootReducer = combineReducers({
    loader,
    activeProduct,
    currentOrder,
    currentViewType,
});

export default rootReducer;