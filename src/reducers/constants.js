/**
* Copyright (c) 2018-2019 PKP Technologies, LLC.  All Rights Reserved.
* 
* This product and related documentation is protected by copyright and
* distributed under licenses restricting its use, copying, distribution and
* decompilation. No part of this product or related documentation may be
* reproduced in any form by any means without prior written authorization
* from PKP Technologies, LLC and/or its licensors.
*/

export const commonConstants = {
    
    SHOW_LOADER: 'SHOW_LOADER',
    HIDE_LOADER: 'HIDE_LOADER',
    
};


