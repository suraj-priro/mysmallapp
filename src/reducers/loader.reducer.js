
import { commonConstants } from './constants';

function loader(state = false, action) {
  switch (action.type) {
    case commonConstants.SHOW_LOADER:
      return true;
    case commonConstants.HIDE_LOADER:
      return false;
    default:
      return state;
  }
}

export default loader;