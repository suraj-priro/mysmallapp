
const OrderInitialState = {
}
const currentOrder=(state=JSON.parse(JSON.stringify(OrderInitialState)),action)=>{
    switch (action.type) {
        case "UPDATE_CURRENT_ORDER":
            return action.payload;
        case "RESET_CURRENT_ORDER":
            return JSON.parse(JSON.stringify(OrderInitialState))    
        default:
            return state;
      }
}


const activeProductInitialState = {
    
}
const activeProduct = (state=JSON.parse(JSON.stringify(activeProductInitialState)),action)=>{
    switch (action.type) {
        case "UPDATE_ACTIVE_PRODUCT":
            return action.payload;
        case "RESET_ACTIVE_PRODUCT":
            return JSON.parse(JSON.stringify(activeProductInitialState))    
        default:
            return state;
      }
}


const currentViewType = (state="9by9",action)=>{
    switch (action.type) {
        case "SET_VIEW_TYPE":
            return action.payload;
        default:
            return state;
      }
}


export {
   currentOrder,
   activeProduct,
   currentViewType,
};