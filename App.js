import 'react-native-gesture-handler';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Amplify from 'aws-amplify'
import config from './aws-exports'
import HomeScreen from './src/screens/HomeScreen/HomeScreen.js'
import MyStore from './src/screens/MyStore/MyStore.js'
import { Provider } from 'react-redux';
import { store } from './src/helpers';
import MyTheme from './src/screens/LoginTheme'
Amplify.configure(config)

import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Loading, ConfirmSignIn, ConfirmSignUp, ForgotPassword, RequireNewPassword, SignIn, SignUp, VerifyContact, withAuthenticator } from 'aws-amplify-react-native';
import CustomSignIn  from './src/screens/Authentication/CustomSignIn';
import CustomForgotPassword  from './src/screens/Authentication/CustomForgotPassword';
import CustomSignUp  from './src/screens/Authentication/CustomSignUp';
import CustomConfirmSignUp  from './src/screens/Authentication/CustomConfirmSignUp';
import CustomLoading  from './src/screens/Authentication/CustomLoading';
import styles from './src/styles/commonStyles'
import Header from './src/screens/Header'
function SettingsScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Settings!</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="MyStores" component={MyStore} />
      <Tab.Screen name="Settings" component={SettingsScreen} />
    </Tab.Navigator>
  );
}

function App() {
  return (
    <Provider store={store} style={{paddingTop:0}}>
      <NavigationContainer style={{paddingTop:0}}>
         <Header></Header> 
        <MyTabs style={{paddingTop:0}}/>
      </NavigationContainer>
    </Provider>

  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });





export default App
// export default withAuthenticator(App, false, [], null, MyTheme)