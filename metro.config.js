const blacklist = require('metro-config/src/defaults/blacklist');
const { getDefaultConfig } = require('metro-config');

module.exports = (async () => { 
	const {
		resolver: {
			sourceExts,
      assetExts,
		}  
	} = await getDefaultConfig(); 

	return {
		transformer: {      
			babelTransformerPath: require.resolve("react-native-svg-transformer"),    
      getTransformOptions: async () => ({
        transform: {
          experimentalImportSupport: false,
          inlineRequires: false,
        },
      }),
    },    
		resolver: {
			assetExts: assetExts.filter(ext => ext !== "svg"),
      sourceExts: [...sourceExts, "svg"],
      blacklistRE: blacklist([/#current-cloud-backend\/.*/]),    
		}};
})();